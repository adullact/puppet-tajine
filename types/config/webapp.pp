# Describes configuration parameters
type Tajine::Config::Webapp = Hash[
  String[1],
  Variant[
    String[1],
    Integer,
    Boolean,
  ],
]
