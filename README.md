# tajine

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with tajine](#setup)
    * [What tajine affects](#what-tajine-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with tajine](#beginning-with-tajine)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

Deploy and configure [Tajine](https://gitlab.adullact.net/adullact/pki/tajine), a registration authority (RA) frontend pluged to backend CFSSL PKI.

## Setup

### What tajine affects

This module install and configure Tajine (a web record authority, working with backend CFSSL certificate authority).

By default, a PostgreSQL server is installed and configured locally, a role and a database are created.

### Setup Requirements

As a web app, you have to configure a web server with php configured. To do so with Puppet, it is possible to use :
 * [puppetlabs-apache](https://forge.puppet.com/modules/puppetlabs/apache/readme)
 * [puppet-nginx](https://forge.puppet.com/modules/puppet/nginx/readme)
 * [puppet-php](https://forge.puppet.com/modules/puppet/php/readme)

As backend, only PostgreSQL is supported. With Puppet, it is possible to use [puppetlabs-postgresql](https://forge.puppet.com/modules/puppetlabs/postgresql/readme).

As certificate authority, only CFSSL is supported. With Puppet, it is possible to use [adullact-cfssl](https://forge.puppet.com/modules/adullact/cfssl/readme).

Tajine send by mail to recipient the produced X.509 certificat in PKCS#12 format.
You have to ensure to set SMTP attributes accordingly with your SMTP infrastructure.
With Puppet, it is possible to use [puppet-postfix](https://forge.puppet.com/modules/puppet/postfix/readme).

### Beginning with tajine

The very basic steps can be simple `include tajine`.

## Usage

If you are using Apache, this setup is possible :

```
class { 'tajine':
  app_secret        => 'ThisIsMySecetUsedToGenerateCSRFTokens',
  smtp_host         => 'smtp.example.org',
  smtp_port         => 465,
  smtp_user         => 'tajine-username%40example.org',
  smtp_password     => 'tajine-smtppassword',
  sys_rootpath      => '/var/www/tajine.example.org',
  sys_rootpath_mode => '0700',
  sys_user          => 'www-data',
  sys_group         => 'www-data',
  webapp            => {
    trusted_hosts => '^tajine.example.org$',
  },
}
```

## Reference

Details are in [REFERENCE.md](https://gitlab.adullact.net/adullact/puppet-tajine/-/blob/main/REFERENCE.md) file.

## Limitations

Supported OSes are given in [metadata.json](https://gitlab.adullact.net/adullact/puppet-tajine/-/blob/main/metadata.json) file.

## Development

Home at URL https://gitlab.adullact.net/adullact/puppet-tajine

Issues and MR are welcome.

## Release Notes/Contributors/Etc.

Details in [CHANGELOG.md](https://gitlab.adullact.net/adullact/puppet-tajine/-/blob/main/CHANGELOG.md).

```
Copyright (C) 2018 Association des Développeurs et Utilisateurs de Logiciels Libres
                     pour les Administrations et Colléctivités Territoriales.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/agpl.html>.

```

