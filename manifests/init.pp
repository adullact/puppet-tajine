# @summary Deploy and configure Tajine, a registration authority pluged to CFSSL PKI.
#
# Deploy and configure Tajine, a registration authority pluged to CFSSL PKI.
#
# @param download_url URL where Tajine archive is stored.
# @param download_checksum Archive file checksum (match checksum_type) used to verify of archive file.
# @param checksum_type Checksum type given with download_checksum.
# @param app_secret Application secret is required to generate CSRF tokens
# @param config_path Directory where Tajine configuration file is stored.
# @param var_path Directory where Tajine stores var files.
# @param sys_usermanage To enable/disable the creation of system user and group. To permit manage then by external process.
# @param sys_user Operating system user account owner of Tajine files
# @param sys_group Operating system group owner of Tajine files
# @param sys_rootpath Path where Tajine is installed
# @param sys_rootpath_mode Unix mode set to path defined by $sys_rootpath
# @param cfssl_host CFSSL host to use
# @param cfssl_port CFSSL port to use
# @param db_manage To enable/disable installation of local PostgreSQl server, creation of role and database. To permit manage then by external process.
# @param db_host PostgreSQL server to use
# @param db_version Version of PostgreSQL server
# @param db_user Database user used to connect PostgreSQL server
# @param db_password Database password used to connect PostgreSQL server
# @param db_name Database name used by Tajine
# @param smtp_host SMTP host to use
# @param smtp_port SMTP port to use
# @param smtp_user SMTP user used with SMTP auth. If user is email, use username%40example.org instead of username@example.org.
# @param smtp_password SMTP password used with SMTP auth
# @param enforce_https To enable/disable enforced https.
# @param webapp 
#   Tajine configuration parameters starting with WEBAPP_ , for all possible parameters, see:
#   https://gitlab.adullact.net/adullact/pki/tajine/-/blob/main/documentation/sysadmin/91_common_configuration.md
#
# @example
#    class { 'tajine':
#      sys_rootpath      => '/var/www/tajine.example.org',
#      sys_rootpath_mode => '0700',
#      sys_user          => 'www-data',
#      sys_group         => 'www-data',
#      webapp            => {
#        trusted_hosts => '^example\.org$|^tajine\.example\.org$',
#      },
#    }
#
class tajine (
  Stdlib::HTTPSUrl $download_url = 'https://gitlab.adullact.net/adullact/pki/tajine/-/package_files/868/download',
  String[1] $download_checksum = '6a2229eb0b9bd5090578989b433597295d9de6e33f150674f26c87115caed7bc',
  Enum['md5', 'sha1', 'sha2', 'sha256', 'sha384', 'sha512'] $checksum_type = 'sha256',
  String[1] $app_secret = 'ThisTokenIsNotSoSecretChangeIt',
  Stdlib::Absolutepath $config_path = '/etc/tajine',
  Stdlib::Absolutepath $var_path = '/var/tajine',
  Boolean $sys_usermanage = true,
  String[1] $sys_user = 'tajine',
  String[1] $sys_group = 'tajine',
  Stdlib::Absolutepath $sys_rootpath = '/opt/tajine',
  String $sys_rootpath_mode = '0750',
  Stdlib::HTTPUrl $cfssl_host = 'http://127.0.0.1',
  Stdlib::Port $cfssl_port = 8080,
  Boolean $db_manage = true,
  Stdlib::Host $db_host = '127.0.0.1',
  String[1] $db_version = '14',
  String[1] $db_user = 'dbtajine',
  Variant[String[1], Sensitive[String]] $db_password = Sensitive.new('changeit'),
  String[1] $db_name = 'tajine',
  Stdlib::Host $smtp_host = '127.0.0.1',
  Stdlib::Port $smtp_port = 25,
  Optional[String[1]] $smtp_user = undef,
  Optional[Variant[String[1], Sensitive[String]]] $smtp_password = undef,
  Boolean $enforce_https = true,
  Tajine::Config::Webapp $webapp = {},
) {
  $_archive_rootdir = 'tajine'

  if $db_manage {
    include postgresql::server

    postgresql::server::db { $db_name:
      user     => $db_user,
      password => postgresql::postgresql_password($db_user, $db_password),
    }

    Postgresql::Server::Db[$db_name] -> Exec['console doctrine:migrations:migrate']
  }

  if $sys_usermanage {
    group { $sys_group :
      ensure => present,
    }
    -> user { $sys_user :
      ensure     => present,
      managehome => true,
      shell      => '/usr/sbin/nologin',
      gid        => $sys_group,
      before     => [
        File[$sys_rootpath],
      ],
    }
  }

  file { $sys_rootpath:
    ensure    => directory,
    mode      => $sys_rootpath_mode,
    owner     => $sys_user,
    group     => $sys_group,
    purge     => true,
    recurse   => true,
    force     => true,
    max_files => 10000,
  }

  archive { 'tajine.tgz':
    path          => "/tmp/tajine-${download_checksum}.tgz",
    source        => $download_url,
    checksum      => $download_checksum,
    checksum_type => $checksum_type,
    extract       => true,
    extract_path  => "${sys_rootpath}/${download_checksum}",
    user          => $sys_user,
    group         => $sys_group,
    creates       => "${sys_rootpath}/${download_checksum}/tajine/composer.lock",
    cleanup       => true,
    require       => File["${sys_rootpath}/${download_checksum}"],
    notify        => Exec['console cache:warmup'],
  }
  -> file { "${sys_rootpath}/${_archive_rootdir}":
    ensure  => link,
    target  => "${sys_rootpath}/${download_checksum}/${_archive_rootdir}",
    require => [
      File[$var_path],
      File["${var_path}/sessions"],
      File["${var_path}/log"],
    ],
  }
  -> file { "${sys_rootpath}/${_archive_rootdir}/config/packages/prod/":
    ensure => directory,
    owner  => $sys_user,
    group  => $sys_group,
    mode   => '0700',
  }

  if $enforce_https {
    file { "${sys_rootpath}/${_archive_rootdir}/config/packages/prod/nelmio_security.yaml":
      ensure  => file,
      owner   => $sys_user,
      group   => $sys_group,
      mode    => '0600',
      content => stdlib::to_yaml({ 'nelmio_security' => { 'forced_ssl' => { 'enabled' => true } } }, { indentation => 4 }),
    }
  } else {
    file { "${sys_rootpath}/${_archive_rootdir}/config/packages/prod/nelmio_security.yaml":
      ensure  => file,
      owner   => $sys_user,
      group   => $sys_group,
      mode    => '0600',
      content => stdlib::to_yaml({ 'nelmio_security' => { 'forced_ssl' => { 'enabled' => false } } }, { indentation => 4 }),
    }
  }

  file { [$config_path, "${sys_rootpath}/${download_checksum}", $var_path, "${var_path}/sessions", "${var_path}/log"]:
    ensure => directory,
    owner  => $sys_user,
    group  => $sys_group,
    mode   => '0700',
  }
  -> file { "${config_path}/env.prod.local":
    ensure  => file,
    owner   => $sys_user,
    group   => $sys_group,
    mode    => '0600',
    content => epp('tajine/env.prod.local.epp'),
  }
  -> file { "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/.env.prod.local":
    ensure  => link,
    target  => "${config_path}/env.prod.local",
    require => Archive['tajine.tgz'],
  }
  -> file { "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/.env.local":
    ensure  => file,
    owner   => $sys_user,
    group   => $sys_group,
    mode    => '0600',
    content => "APP_ENV=prod\n",
  }
  -> file { "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/var":
    ensure  => directory,
    owner   => $sys_user,
    group   => $sys_group,
    mode    => '0700',
    require => Archive['tajine.tgz'],
  }
  -> file { "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/var/log":
    ensure  => link,
    target  => "${var_path}/log",
    require => Archive['tajine.tgz'],
  }
  -> file { "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/var/sessions":
    ensure  => link,
    target  => "${var_path}/sessions",
    require => Archive['tajine.tgz'],
  }

  exec { 'console cache:warmup':
    command     => "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/bin/console cache:warmup",
    cwd         => "${sys_rootpath}/${download_checksum}/${_archive_rootdir}",
    user        => $sys_user,
    group       => $sys_group,
    environment => [
      'APP_ENV=prod',
      'APP_DEBUG=0',
    ],
    refreshonly => true,
    require     => File["${sys_rootpath}/${download_checksum}/${_archive_rootdir}/var"],
  }

  exec { 'console doctrine:migrations:migrate':
    command => "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/bin/console doctrine:migrations:migrate --env=prod --no-interaction --no-ansi",
    cwd     => "${sys_rootpath}/${download_checksum}/${_archive_rootdir}",
    user    => $sys_user,
    group   => $sys_group,
    unless  => "${sys_rootpath}/${download_checksum}/${_archive_rootdir}/bin/console doctrine:migrations:up-to-date --env=prod --no-interaction --no-ansi",
    require => [
      Archive['tajine.tgz'],
      Exec['console cache:warmup'],
    ],
  }
}
