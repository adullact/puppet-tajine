require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper
install_module_on(hosts)
install_module_dependencies_on(hosts)

php_version = '8.1'
sys_rootpath = '/var/www/tajine'
archive_rootdir = 'tajine'
public_dir = 'public'

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    # Additional module and configuration for being able to setup web server requirements
    install_module_from_forge('puppetlabs-apache', '>= 8.0.0 < 13.0.0')

    pp_helper = %(
      exec { 'mkdir #{sys_rootpath}':
        command => '/usr/bin/mkdir -p #{sys_rootpath}',
      }
      -> exec { 'chown www-data #{sys_rootpath}':
        command => '/usr/bin/chown -R www-data:www-data #{sys_rootpath}',
      }
      -> file { '#{sys_rootpath}/#{archive_rootdir}/':
        ensure => 'link',
        target => '#{sys_rootpath}',
      }
      -> class { 'apache':
        default_vhost => false,
        default_mods  => ['php', 'rewrite'],
        mpm_module    => 'prefork',
      }
      apache::vhost { 'tajine':
        port          => 80,
        docroot       => '#{sys_rootpath}/#{archive_rootdir}/#{public_dir}',
        docroot_owner => 'www-data',
        docroot_group => 'www-data',
        override      => ['All'],
      }

      $php_libs = [
        'php#{php_version}-curl',
        'php#{php_version}-intl',
        'php#{php_version}-pgsql',
        'php#{php_version}-mbstring',
        'php#{php_version}-xml',
      ]
      package { $php_libs:
        ensure  => present,
      }
    )
    apply_manifest(pp_helper, catch_failures: true)
  end
end
