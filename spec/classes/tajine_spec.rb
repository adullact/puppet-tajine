# frozen_string_literal: true

require 'spec_helper'

sys_rootpath = '/opt/tajine'
archive_rootdir = 'tajine'

describe 'tajine' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(service_provider: 'systemd') }

      it { is_expected.to compile }
      it { is_expected.to contain_archive('tajine.tgz') }

      context 'without smtp auth' do
        it do
          is_expected.to contain_file('/etc/tajine/env.prod.local') \
            .with_content(sensitive(%r{^MAILER_DSN=smtp://127.0.0.1:25$}))
        end
      end

      context 'with smtp auth' do
        let(:params) do
          {
            smtp_host: 'smtp.example.org',
            smtp_port: 465,
            smtp_user: 'mysmtpuser',
            smtp_password: 'mysmtppwd',
          }
        end

        it do
          is_expected.to contain_file('/etc/tajine/env.prod.local') \
            .with_content(sensitive(%r{^MAILER_DSN=smtp://mysmtpuser:mysmtppwd@smtp.example.org:465$}))
        end
      end

      context 'with enforce_https true' do
        let(:params) do
          {
            enforce_https: true,
          }
        end

        it do
          is_expected.to contain_file("#{sys_rootpath}/#{archive_rootdir}/config/packages/prod/nelmio_security.yaml") \
            .with_content(%r{^nelmio_security:\n\s+forced_ssl:\n\s+enabled:\strue$})
        end
      end

      context 'with enforce_https to false' do
        let(:params) do
          {
            enforce_https: false,
          }
        end

        it do
          is_expected.to contain_file("#{sys_rootpath}/#{archive_rootdir}/config/packages/prod/nelmio_security.yaml") \
            .with_content(%r{^nelmio_security:\n\s+forced_ssl:\n\s+enabled:\sfalse$})
        end
      end

      context 'with trusted_hosts' do
        let(:params) do
          {
            webapp: {
              trusted_hosts: '^example.org$',
            },
          }
        end

        it do
          is_expected.to contain_file('/etc/tajine/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_TRUSTED_HOSTS='\^example\.org\$'$}))
        end
      end

      context 'with locale fr' do
        let(:params) do
          {
            webapp: {
              i18n_default_locale: 'fr',
            },
          }
        end

        it do
          is_expected.to contain_file('/etc/tajine/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_I18N_DEFAULT_LOCALE='fr'}))
        end
      end

      context 'with custom min password length' do
        let(:params) do
          {
            webapp: {
              user_config_min_password_length: 16,
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/tajine/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH=16$}))
        end
      end
    end
  end
end
