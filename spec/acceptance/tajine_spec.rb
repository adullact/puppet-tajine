require 'spec_helper_acceptance'

sys_rootpath = '/var/www/tajine'
download_checksum_initial = 'eed0e41dbba67845653cc36cc038bb55a662cd89a680c8489b04201fb2c061e5'
download_url_initial = 'https://gitlab.adullact.net/adullact/pki/tajine/-/package_files/859/download'
download_checksum_upgrade = '6a2229eb0b9bd5090578989b433597295d9de6e33f150674f26c87115caed7bc'
download_url_upgrade = 'https://gitlab.adullact.net/adullact/pki/tajine/-/package_files/868/download'
config_path = '/etc/tajine'
archive_rootdir = 'tajine'
var_path = '/var/tajine'
networking_domain = fact('networking.domain')

describe 'tajine' do
  context 'with basic settings' do
    pp = %(
      class { 'tajine':
        download_url      => '#{download_url_initial}',
        download_checksum => '#{download_checksum_initial}',
        sys_rootpath      => '#{sys_rootpath}',
        sys_rootpath_mode => '0700',
        sys_user          => 'www-data',
        sys_group         => 'www-data',
        enforce_https     => false,
        webapp            => {
          trusted_hosts       => '127.0.0.1',
          name                => 'TAJINE PUPPET MODULE CI-A',
          shortname           => 'TAJINE CI',
          smtp_mailfrom       => "admin@#{networking_domain}",
          smtp_mailalertingto => "monitor@#{networking_domain}",
        },
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe file("#{sys_rootpath}/#{archive_rootdir}") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}" }
    end

    describe command('curl http://127.0.0.1') do
      its(:stdout) { is_expected.to match %r{<title>TAJINE PUPPET MODULE CI-A</title>} }
    end

    describe command('curl --head http://127.0.0.1/health-check') do
      its(:stdout) { is_expected.to match %r{x-tajine-database-status: DB_CONNECTION_SUCCESSFUL} }
    end

    describe file(config_path.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{config_path}/env.prod.local") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 600 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
      its(:content) { is_expected.to contain "WEBAPP_TRUSTED_HOSTS='127.0.0.1'" }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/.env.local") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 600 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
      its(:content) { is_expected.to contain 'APP_ENV=prod' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/.env.prod.local") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{config_path}/env.prod.local" }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var") do
      it { is_expected.not_to be_symlink }
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/cache") do
      it { is_expected.not_to be_symlink }
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/cache/prod") do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/log") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{var_path}/log" }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/sessions") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{var_path}/sessions" }
    end

    describe file(var_path.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{var_path}/cache") do
      it { is_expected.not_to be_directory }
    end

    describe file("#{var_path}/log") do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{var_path}/sessions") do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end
  end

  context 'with upgrade' do
    pp = %(
      class { 'tajine':
        download_url      => '#{download_url_upgrade}',
        download_checksum => '#{download_checksum_upgrade}',
        sys_rootpath      => '#{sys_rootpath}',
        sys_rootpath_mode => '0700',
        sys_user          => 'www-data',
        sys_group         => 'www-data',
        enforce_https     => false,
        webapp            => {
          trusted_hosts       => '127.0.0.1',
          name                => 'TAJINE PUPPET MODULE CI-B',
          shortname           => 'TAJINE CI',
          smtp_mailfrom       => "admin@#{networking_domain}",
          smtp_mailalertingto => "monitor@#{networking_domain}",
        },
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe command('curl http://127.0.0.1') do
      its(:stdout) { is_expected.to match %r{<title>TAJINE PUPPET MODULE CI-B</title>} }
    end

    describe command('curl --head http://127.0.0.1/health-check') do
      its(:stdout) { is_expected.to match %r{x-tajine-database-status: DB_CONNECTION_SUCCESSFUL} }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}") do
      it { is_expected.not_to exist }
    end
  end
end
