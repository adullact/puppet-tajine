# frozen_string_literal: true

require 'spec_helper'

describe 'Tajine::Config::Webapp' do
  [
    { 'param1' => 'this is a string' },
    { 'param2' => 42 },
    { 'param3' => true },
  ].each do |allowed_value|
    it { is_expected.to allow_value(allowed_value) }
  end

  [
    { 'array fail' => [ '1', '2' ] },
    { 'hash fail' => { '1' => '2' } },
  ].each do |invalid_value|
    it { is_expected.not_to allow_value(invalid_value) }
  end
end
