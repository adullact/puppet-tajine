# Changelog

All notable changes to this project will be documented in this file.

## Release 4.1.0

 * parameter 'webapp' entry 'session_lifetime' expects a String value, got Integer #51

## Release 4.0.0

 * Test : simplify spec_helper_acceptance.rb #49
 * Allow to enable/disable required HTTPS #48
 * update REFERENCE.md #47
 * Allow to configure WEBAPP_DEFAULT_URI environment variable #42
 * Use Hash data type parameter for all webapp configs instead a liste of parameters #30
 * Add missing environment variables #28
 
## Release 3.0.0

* fix release task #45
* update version constraints for puppetlabs-stdlib: ">= 9.0.0 < 10.0.0" #41
* use PDK 3.2.0 and Ruby 3.2.0 #40

## Release 2.0.0

 * File resource used .env.prod.local instead of .env.prod #29
 * Drop puppet 6 support #31
 * Drop warning by changing max_files attribute of file resource to 10000 #32
 * Add i18ndefaultlocale parameter #33
 * Enforce APP_ENV to prod #34
 * Defaults i18ndefaultlocale to en #35
 * Defaults Tajine to 0.14.0 #37

## Release 1.1.1

* old version of tajine are not cleaned #22

## Release 1.1.0

* Fix incompatible cache between two versions of Tajine  #21
* metadata.json - Add issues_url and project_page #23
* Add some acceptance tests #24
* Default deployed tajine version to 0.10.0  #25

## Release 1.0.0

Initial release.

